package task00

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import scala.util.Random
import scala.util.Try

class RationalTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }
  
  "Rat(1,0)" should "kivételt kéne dobjon" in {
    assertThrows[IllegalArgumentException] { val x = Rat(1,0) }
  }
  
  "toString" should "-?a/b alakú kéne legyen" in {
    for {
      a <- -10 to 10
      b <- -10 to 10 if b != 0
    } {
      val expected = (if (a * b < 0) "-" else "") + s"${Math.abs(a)}/${Math.abs(b)}"
      val result = Try(Rat(a,b).toString)
      val testString = s"teszteset: Rat($a,$b), kéne: $expected, lett: "
      assert(result.isSuccess, () => testString + s"kivétel ${result.failed.get}")
      assert(result.get == expected, testString + s"${result.get}")
    }
  }


  "gcd" should "a legnagyobb közös osztót kéne visszaadja" in {
    val primes = Vector(2,3,5,7,11,13,17,19)
    for (_ <- 1 to 1000)
    {
      val d = Random.nextInt(100) + 1
      val init = (if (Random.nextBoolean()) d else -d, if (Random.nextBoolean()) d else -d)
      val (a,b) = primes.foldLeft(init)((pair,prime) => {
        val dice = Random.nextInt(3)
        dice match {
          case 0 => pair
          case 1 => (pair._1 * prime, pair._2)
          case 2 => (pair._1, pair._2 * prime)
        }
      })      
      val actual = MyMath.gcd(a,b)
      assert( actual == d, s"teszteset: $a, $b, kéne: $d, lett: $actual")
      assert( MyMath.gcd(a, 0) == Math.abs(a), s"teszteset: $a, 0, kéne: ${Math.abs(a)}, lett: ${MyMath.gcd(a,0)}" )
      assert( MyMath.gcd(0, a) == Math.abs(a), s"teszteset: 0, $a, kéne: ${Math.abs(a)}, lett: ${MyMath.gcd(0,a)}" )
    }
  }
  
  "egyenlőség" should "nem egyszerűsített alakra" in {
    for (_ <- 1 to 1000)
    {
      val a = Random.nextInt(100)
      val b = Random.nextInt(100) + 1
      val szamlalo = if (Random.nextBoolean()) a else -a
      val nevezo = if (Random.nextBoolean()) b else -b
      val c = Random.nextInt(10) + 1
      val multiplier = if (Random.nextBoolean()) c else -c
      val actual = Rat(szamlalo, nevezo) == Rat(szamlalo * multiplier, nevezo * multiplier)
      assert(actual, s"teszteset: Rat($szamlalo,$nevezo) és Rat(${szamlalo * multiplier},${nevezo * multiplier}) egyenlő kéne legyen")
    }
    for (_ <- 1 to 1000)
    {
      val a = Random.nextInt(100)
      val b = Random.nextInt(100) + 1
      val szamlalo = if (Random.nextBoolean()) a else -a
      val nevezo = if (Random.nextBoolean()) b else -b
      val actual = Rat(szamlalo, nevezo) == Rat(szamlalo + 1, nevezo)
      assert(!actual, s"teszteset: Rat($szamlalo,$nevezo) és Rat(${szamlalo + 1},$nevezo) nem kéne egyenlő legyen")
    }
  }
  
  it should "túlcsordulásra" in {
    val a = (1 << 16) + 1
    val b = 3
    val c = 43691
    val d = a
    val actual = Rat(a,b) == Rat(c,d)
    assert(!actual, s"teszteset: Rat($a, $b) és Rat($c, $d) nem kéne egyenlő legyen")
  }
  
  "szorzás" should "néhány konstansra" in {
    for {
      a1 <- -10 to 10
      b1 <- -10 to 10 if b1 != 0
      a2 <- -10 to 10
      b2 <- -10 to 10 if b2 != 0
    } {
      val rat1 = Rat(a1,b1)
      val rat2 = Rat(a2,b2)
      val result = rat1 * rat2
      val expected = Rat(a1 * a2, b1 * b2)
      assert(result == expected, s"Rat($a1, $b1) * Rat($a2, $b2) == Rat(${expected.a}, ${expected.b}) kéne legyen, lett Rat(${result.a},${result.b})")
    }
  }
  
  "szorzás" should "túlcsordulásra" in {
    val left = Rat(1 << 10, 1 << 20)
    val right = Rat(1 << 10, 1 << 20)
    val expected = Rat(1, 1 << 20)
    val result = scala.util.Try[Rat](left * right)
    assert(result.isSuccess && result.get == expected,
    s"Rat(${left.a}, ${left.b}) * Rat(${right.a}, ${right.b}) == Rat(${expected.a}, ${expected.b}) kéne legyen, lett ${result.getOrElse("kivétel")}")
  }
  
  "összeadás" should "néhány konstansra" in {
    for {
      a1 <- -10 to 10
      b1 <- -10 to 10 if b1 != 0
      a2 <- -10 to 10
      b2 <- -10 to 10 if b2 != 0
    } {
      val rat1 = Rat(a1,b1)
      val rat2 = Rat(a2,b2)
      val result = rat1 + rat2
      val expected = Rat(a1 * b2 + a2 * b1, b1 * b2)
      assert(result == expected, s"Rat($a1, $b1) * Rat($a2, $b2) == Rat(${expected.a}, ${expected.b}) kéne legyen, lett Rat(${result.a},${result.b}")
    }
  }
  
  "összeadás" should "túlcsordulásra" in {
    val left = Rat(1, 1 << 20)
    val right = left
    val expected = Rat(1, 1 << 19)
    val result = scala.util.Try[Rat](left + right)
    assert(result.isSuccess && result.get == expected, s"Rat(${left.a},${left.b}) + Rat(${right.a},${right.b}) == Rat(${expected.a},${expected.b}) kéne legyen, lett ${result.getOrElse("kivétel: " + result.failed.get)}")
  }
  
  
}

