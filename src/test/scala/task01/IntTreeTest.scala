package task01

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import scala.util.Random

class IntTreeTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }
  
  "ContainsTest" should "for empty tree" in {
    for (i <- -5 to 5) {
      assert(!(EmptyTree contains i), s"az üres fa nem kellene tartalmazza $i-t")
    }
  }
  it should "for a nonempty tree" in {
    val tree = BinaryTree(BinaryTree(EmptyTree, -3, EmptyTree), 0, BinaryTree(BinaryTree(EmptyTree,3,EmptyTree),6,EmptyTree))
    for (i <- -5 to 5) {
      val expected = Vector(-3,0,3,6) contains i
      val result = tree contains i
      assert(result == expected, s"-3.0.(3.6.)-ra a contains($i) hibás eredményt adott")
    }
  }
  
  "AddTest" should "for empty tree" in {
    for (i <- -5 to 5) {
      val result = EmptyTree + i
      val expected = BinaryTree(EmptyTree, i, EmptyTree)
      assert(result == expected, s"EmptyTree + $i eredménye egy egycsúcsú fa kéne legyen, lett $result")
    }
  }
  
  it should "for adding 1,4,2,8,5,7" in {
    val input = Vector(1,4,2,8,5,7)
    val result = input.foldLeft[IntTree](EmptyTree)((tree, value) => tree + value)
    for (i <- -5 to 10) {
      assert(result.contains(i) == input.contains(i), s"Beszúrva az 1, 4, 2, 8, 5, 7 elemeket a kapott fára contains($i) hibás!")
    }
  }
  
  "ForeachTest" should "varosan" in {
    var sum = 0
    val inputTree = BinaryTree(BinaryTree(EmptyTree, -3, EmptyTree), 1, BinaryTree(BinaryTree(EmptyTree,3,EmptyTree),6,EmptyTree))
    for (data <- inputTree) { sum += data }
    assert(sum == 7, s"foreach-ben varba szummázva -3.1.(3.6.)-ot az eredmény $sum lett, nem pedig 7")
  }
  
  "FilterTest" should "3-mal oszthatóakra" in {
    val inputTree = BinaryTree(BinaryTree(EmptyTree, -3, EmptyTree), 1, BinaryTree(BinaryTree(EmptyTree,3,EmptyTree),5,EmptyTree))
    val result = inputTree filter { _ % 3 == 0 }
    val expected = BinaryTree(EmptyTree, -3, BinaryTree(EmptyTree, 3, EmptyTree))
    val expected2 = BinaryTree(BinaryTree(EmptyTree,-3,EmptyTree),3,EmptyTree)
    assert(result == expected || result == expected2, s"-3.1.(3.5.)-öt filterezve a hárommal oszthatóakra az eredmény $result lett")
  } 
  
  "ExistsTest" should "találatra" in {
     val inputTree = BinaryTree(BinaryTree(EmptyTree, -3, EmptyTree), 1, BinaryTree(BinaryTree(EmptyTree,3,EmptyTree),5,EmptyTree))
     val result = inputTree.exists( _ == 5 )
     assert(result, s"-3.1.(3.5.)-re exists( _ == 5) igaz kéne legyen")
  }
  it should "nem találatra" in {
     val inputTree = BinaryTree(BinaryTree(EmptyTree, -3, EmptyTree), 1, BinaryTree(BinaryTree(EmptyTree,3,EmptyTree),5,EmptyTree))
     val result = inputTree.exists( _ == 4 )
     assert(!result, s"-3.1.(3.5.)-re exists( _ == 4) hamis kéne legyen")
  }
  
  "CountTest" should "mod 3-ra" in {
     val inputTree = BinaryTree(BinaryTree(EmptyTree, -3, EmptyTree), 1, BinaryTree(BinaryTree(EmptyTree,3,EmptyTree),5,EmptyTree))
     val result = inputTree.count( _ % 3 == 0 )
     assert(result == 2, s"-3.1.(3.5.)-re count( _ + 3 == 0) 2 kéne legyen, lett $result")
  }
  "MapTest" should "*(-1)-re" in {
    val inputTree = BinaryTree(BinaryTree(EmptyTree, -3, EmptyTree), 1, BinaryTree(BinaryTree(EmptyTree,3,EmptyTree),5,EmptyTree))
    val result = inputTree map { _ * -1 }
    val datas = Vector(-3,1,3,5)
    for (i <- -5 to 5) {
      assert(datas.contains(i) == result.contains(-i), s"-3.1.(3.5.) map { _ * -1 } -ben contains(${-i}) hibás eredményt ad")
    }    
  }
  "FoldTest" should "sumra" in {
    val inputTree = BinaryTree(BinaryTree(EmptyTree, -3, EmptyTree), 1, BinaryTree(BinaryTree(EmptyTree,3,EmptyTree),5,EmptyTree))
    val result = inputTree.fold(10)( 2 * _ + _ )
    val expected = 2 * ( 2 * (2 * (2 * 10 + -3 ) + 1) + 3) + 5
    assert(result == expected, s"-3.1.(3.5.) fold(10)( 2 * _ + _ ) eredménye $expected kéne legyen, lett $result")
   }
   
   "PlusPlusTest" should "két vektorra" in {
     val input1 = Vector(1,4,2,8,5,7)
     val input2 = Vector(2,5,3,9,6,8)
     val tree1 = input1.foldLeft[IntTree](EmptyTree)((tree,data) => tree + data)
     val tree2 = input2.foldLeft[IntTree](EmptyTree)((tree,data) => tree + data)
     val result = tree1 ++ tree2
     for (i <- 0 to 20) {
       assert(result.contains(i) == input1.contains(i) || input2.contains(i), s"az (1,4,2,8,5,7) és a (2,5,3,9,6,8) vektorokból épített fák ++-ára a contains($i) hibás")
     }
   }
}

