package task01

/* Bináris keresőfát fogunk implementálni.
 * - bináris fa: minden csúcsának lehet egy "bal" és egy "jobb" fia
 * - lesz egy "üres fa", ami már nem tárol adatot és nincsenek gyerekei
 * - a többi fa mind BinaryTree, bal gyerekkel, jobb gyerekkel és egy Int adatmezővel
 * - attól keresőfa, hogy a bal gyerekben az összes tárolt adat mindig kisebb, mint a gyökérben
 *   és a jobb gyerekben az összes tárolt adat mindig nagyobb, mint a gyökérben
 * - Int-ek halmazát lehet benne rendezetten tárolni
 **/

trait IntTree {
  /* Implementáljunk egy "contains" metódust, ami megmondja, hogy a fában valahol szerepel-e a megadott adat!
   * Pl. BinaryTree( BinaryTree( EmptyTree, 3, EmptyTree ), 5, EmptyTree )-re a contains(3) és a contains(5) true,
   * minden másra false.
   **/
  def contains(value: Int): Boolean = ???
  
  /* Implementáljunk egy + metódust, ami visszaad egy új fát, ami annyiban különbözik az eredetitől, hogy
   * ha az eredetiben nincs benne a value, akkor beleteszi azt is! (ha benne van, akkor ne változzon a fa)
   * Pl. EmptyTree + 3 értéke BinaryTree(EmptyTree, 3, EmptyTree) legyen,
   * BinaryTree(EmptyTree, 3, EmptyTree) + 5 értéke pedig (mondjuk) BinaryTree(EmptyTree, 3, BinaryTree(EmptyTree, 5, EmptyTree)).
   * note: ehhez a teszthez szükséges egy helyesen működő contains metódus is.
   **/
  def +(value: Int): IntTree = ???
   
   /* Implementáljunk egy foreach metódust, ami kap egy f: Int => Any függvényt és minden, a fában szereplő adaton
    * kiértékeli f-et! Visszatérési értéke legyen Unit.
    **/
  def foreach(f: Int => Any): Unit = ???
  
  /* Implementáljunk egy exists metódust, ami kap egy p: Int => Boolean predikátumot és visszaadja, hogy szerepel-e
   * a fában legalább egy olyan data érték, melyre igaz p!
   **/
  def exists(p: Int => Boolean): Boolean = ???
  
  /* Implementáljunk egy count metódust, ami kap egy p: Int => Boolean predikátumot és visszaadja, hogy a fában
   * mennyi olyan adat van, melyre igaz p!
   **/
  def count(p: Int => Boolean): Int = ???
  
  /* Implementáljunk egy fold metódust, ami kap egy kezdőértéket és egy f:(Int,Int) -> Int függvényt, és
   * növekvő sorrendben bejárva a fát, foldolja az értéket!
   * pl. fold(0)(_+_) összegezze a fában az adatok értékeit.
   **/
  def fold(base: Int)(op: (Int,Int) => Int): Int = ???
  
  /* Implementáljunk egy ++ metódust, ami visszaadja this és that unióját!
   * note: ehhez a teszthet kell egy működő + operátor és egy működő contains metódus.
   **/
  def ++(that: IntTree): IntTree = ???
  
  /* Implementáljunk egy map metódust, ami kap egy f: Int => Int függvényt és előállít egy olyan IntTree-t,
   * melyben az összes olyan f(data) szerepel, ami data benne volt a fában!
   * note: ehhez a teszthez kell egy működő contains metódus.
   **/
  def map(f: Int => Int): IntTree = ???
  
  /* Implementáljunk egy filter metódust, ami kap egy p: Int => Boolean predikátumot és előállít egy olyan IntTree-t,
   * melyben pont azok az elemek szerepelnek, akik szerepeltek a fában és igaz rájuk p!
   **/
  def filter(p: Int => Boolean): IntTree = ???
}

/* Ha úgy látjuk jónak, a fenti függvények implementációit tehetjük a leszármazott osztályokba is.
 **/
case object EmptyTree extends IntTree 

case class BinaryTree(left: IntTree, data: Int, right: IntTree) extends IntTree 

// ezekhez nincsenek tesztek, ha eljutunk idáig, írjunk pár saját tesztet is!
object TreeApp extends App {
  /* Írjunk olyan függvényt, mely kiírja konzolra a kapott IntTree-beli páros számokat!
   **/
  def printOddValues(tree: IntTree): Unit = ???
  
  /* Írjunk olyan függvényt, mely az input IntTree-ből készít egy olyat, melyben az eredeti
   * fa-beli adatok ellentettjei vannak!
   **/
  def changeSigns(tree: IntTree): IntTree = ???
  
  /* Írjunk olyan függvényt, mely az input List[Int] elemeit (balról jobbra) egyesével beszúrja egy
   * IntTree-be, és adjuk vissza az eredményként kapott IntTree-t!
   **/
  def listToTree(list: List[Int]): IntTree = ???
}


