package task00

object MyMath {
  /* Implementáljuk a "legnagyobb közös osztó" függvényt:
   * az input a, b Int-ekre adjuk vissza azt a legnagyobb d intet,
   * melyre d osztja a-t is és b-t is!
   *
   * Ha a is és b is 0, bármit visszaadhatunk.
   **/
  def gcd(a: Int, b: Int): Int = ???
}

/**
 * Implementáljunk egy racionális számokat tároló osztályt!
 * Két mezeje legyen a és b, a szám amit tárol, legyen a / b.
 */
case class Rat(a: Int, b: Int) {
  /* Ha a példányosításkor b == 0, akkor dobjunk egy IllegalArgumentException -t!
   **/
  
   /* Érjük el, hogy a Rat osztály toString metódusa az "a/b" stringet adja vissza,
    * az alapértelmezetten generált Rat(a,b) helyett! Ha a hányados negatív, legyen az
    * előjel elöl, szóköz nélkül, ha nemnegatív, akkor ne legyen előjel.
    */
     
   /* Implementáljunk egyenlőség operátort két racionális szám közt!
    * pl.
    * Rat(2,3) == Rat(4,6)   // true
    * Rat(-2,3) == Rat(2,-3) // true
    * Rat(1,2) == Rat(1,3)   // false
    */

  def ==(that: Rat): Boolean = ???
  
  /* Implementáljunk szorzás operátort két racionális szám közt!
   * pl.
   * val a = Rat(2,3) // 2/3
   * val b = Rat(4,7) // 4/7
   * val c = a * b    // Rat(8,21)
   * note: ennek a tesztjéhez az == operátornak helyesen kell működnie.
   **/
  def *(that: Rat): Rat = ???
    
    /* Implementáljunk összeadás operátort két racionális szám közt!
     * pl.
     * val a = Rat(1,3) + Rat(1,2) // Rat(5,6)
     * note: ennek a tesztjéhez az == operátornak helyesen kell működnie.
     */
  def +(that: Rat): Rat = ???

}

object Rational extends App {
  // ha ki akarjuk próbálni, hogy azt csinálja-e az implementációnk, amit szeretnénk,
  // ide tudunk irkálni pl. debug printeket.
}
